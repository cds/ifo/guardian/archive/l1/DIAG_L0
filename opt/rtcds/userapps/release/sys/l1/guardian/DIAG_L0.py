from SYS_DIAG import *

####################
@SYSDIAG.register
def TEMP_LVEA():
    """Check LVEA temperature is within defined thresholds

    """
    max_threshold = 67.9
    min_threshold = 66.9

    if ezca['FMC-CS_LVEA_AVTEMP'] > max_threshold or ezca['FMC-CS_LVEA_AVTEMP'] < min_threshold:
        yield "LVEA temperature out of range!"
####################

####################
@SYSDIAG.register
def TEMP_VEAX():
    """Check VEAX temperature is within defined thresholds

    """
    max_threshold = 68.5 # 67.5 With VEA at 66F, increased to 68F 06/30/2020 - SMA
    min_threshold = 67.5 # 66.5 With VEA at 66F, increased to 68F 06/30/2020 - SMA

    if ezca['FMC-EX_VEA_302_AVTEMP'] > max_threshold or ezca['FMC-EX_VEA_AVTEMP'] < min_threshold:
        yield "VEAX temperature out of range!"
####################

####################
@SYSDIAG.register
def TEMP_VEAY():
    """Check VEAY temperature is within defined thresholds

    """
    max_threshold = 68.5 # 67.7 With VEA at 66F, increased to 68F 06/30/2020 - SMA
    min_threshold = 67.5 # 66.7 With VEA at 66F, increased to 68F 06/30/2020 - SMA

    if ezca['FMC-EY_VEA_202_AVTEMP'] > max_threshold or ezca['FMC-EY_VEA_AVTEMP'] < min_threshold:
        yield "VEAY temperature out of range!"
####################

####################
@SYSDIAG.register
def H2O_LVEA():
    """Check H2O chiller temperature in LVEA is below threshold

    """
    threshold = 48.0

    if ezca['FMC-CS_CY_H2O_SUP_TEMP'] > threshold:
        yield "LVEA H2O chiller temperature out of range!"
####################

####################
@SYSDIAG.register
def H2O_VEAX():
    """Check H2O chiller temperature in VEAX is below threshold

    """
    threshold = 46.0 

    if ezca['FMC-EX_CY_H2O_SUP_TEMP'] > threshold:
        yield "VEAX H2O chiller temperature out of range!"
####################

####################
@SYSDIAG.register
def H2O_VEAY():
    """Check H2O chiller temperature in VEAY is below threshold

    """
    threshold = 50.0

    if ezca['FMC-EY_CY_H2O_SUP_TEMP'] > threshold:
        yield "VEAY H2O chiller temperature out of range!"
####################

####################
@SYSDIAG.register
def AHU_LVEA():
    """Check AHU discharge temperature in LVEA is below threshold

    """
    threshold = 69.0

    if (bool(ezca['FMC-CS_AHU1_FAN1_SWITCH']) or bool(ezca['FMC-CS_AHU1_FAN2_SWITCH'])) & (bool(ezca['FMC-CS_AHU1_DISCH_TEMP'] > threshold)):
        yield "LVEA AHU1 discharge FAN1/FAN2 temperature out of range!"
        
    if (bool(ezca['FMC-CS_AHU2_FAN3_SWITCH']) or bool(ezca['FMC-CS_AHU2_FAN4_SWITCH'])) & (bool(ezca['FMC-CS_AHU2_DISCH_TEMP'] > threshold)):
        yield "LVEA AHU2 discharge FAN3/FAN4 temperature out of range!"
        
    if (bool(ezca['FMC-CS_AHU3_FAN5_SWITCH']) or bool(ezca['FMC-CS_AHU3_FAN6_SWITCH'])) & (bool(ezca['FMC-CS_AHU3_COIL_DISCH_TEMP'] > threshold)):
        yield "LVEA AHU3 discharge FAN5/FAN6 temperature out of range!"    
        
####################

####################
@SYSDIAG.register
def AHU_VEAX():
    """Check AHU discharge temperature in VEAX is below threshold

    """
    threshold = 70.0

    if bool(ezca['FMC-EX_AHU_FAN1_SWITCH']) & (ezca['FMC-EX_AHU_FAN1_DISCH_TEMP'] > threshold):
        yield "VEAX AHU discharge FAN1 temperature out of range!"
        
    if bool(ezca['FMC-EX_AHU_FAN2_SWITCH']) & (ezca['FMC-EX_AHU_FAN2_DISCH_TEMP'] > threshold):
        yield "VEAX AHU discharge FAN2 temperature out of range!"
####################

####################
@SYSDIAG.register
def AHU_VEAY():
    """Check AHU discharge temperature in VEAY is below threshold

    """
    threshold = 69.0

    if bool(ezca['FMC-EY_AHU_FAN1_SWITCH']) & (ezca['FMC-EY_AHU_FAN1_DISCH_TEMP'] > threshold):
        yield "VEAY AHU discharge FAN1 temperature out of range!"
        
    if bool(ezca['FMC-EY_AHU_FAN2_SWITCH']) & (ezca['FMC-EY_AHU_FAN2_DISCH_TEMP'] > threshold):
        yield "VEAY AHU discharge FAN2 temperature out of range!"
####################
